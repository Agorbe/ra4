import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestCliente {
static GestorContabilidad unGestor;
 Cliente uncliente;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("Empezando tests");
		unGestor=new GestorContabilidad();
		Cliente uncliente=new Cliente("Arturo","73131657Y",0);
	}
@Before
public void limpiar(){
	unGestor.getV2().clear();
	
}
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("Acabando tests");
	}

	//No encuentra ningun cliente con ese dni
	@Test
	public void testBuscarCliente() {
		
		unGestor.getV2().add(uncliente);
		
		
		Cliente esperado=unGestor.buscarCliente("A2");
		assertNull(esperado);
	}
	//Se encuentra al menos a un cliente con ese dni
	@Test
	public void testBuscarCliente2() {
	
		String dni="73131657Y";
		
		Cliente esperado=unGestor.buscarCliente(dni);
		assertSame(uncliente,esperado);
	}
	@Test //Comprueba que se a�ade un cliente nuevo al array
	public void testaltaCliente(){
		Cliente otrocliente=new Cliente("Alberto","11131657Y",21/8/2018);
		unGestor.altaCliente(otrocliente);
		unGestor.getV2().add(otrocliente);
		boolean actual=unGestor.getV2().contains(otrocliente);
		
		assertTrue(actual);
	}
	@Test //se a�ade un cliente con el mismo dni y se comprueba
	public void testaltaCliente2(){
		Cliente uncliente=new Cliente("Antonio","11131657Y",21/8/2018);
		unGestor.altaCliente(uncliente);
		
		boolean actual=unGestor.getV2().contains(uncliente);
		
		assertFalse(actual);
	}
	@Test //Se comprueba el cliente mas antiguo
	public void testmasantiguo(){
		Cliente cliente1=new Cliente("Arturo","73131657Y",2/06/2018);
		Cliente cliente2=new Cliente("Antonio","66666666X",3/06/2018);
		unGestor.getV2().add(cliente1);
		unGestor.getV2().add(cliente2);
		Cliente esperado=unGestor.clienteMasAntiguo();
		assertNotEquals(cliente1, esperado);
	}
	@Test //Se comprueba el cliente mas antiguo con uno nuevo
	public void testmasantiguo2(){

		Cliente cliente3=new Cliente("Juan","555555X",8/04/2018);
		
		unGestor.getV2().add(cliente3);
		Cliente actual=unGestor.clienteMasAntiguo();
		Cliente nuevo=unGestor.buscarCliente("555555X");
		assertEquals(nuevo, actual);
	}
	//se a�ade un cliente y se comprueba que puede eliminarse poniendo el DNI
	@Test
	public void testeliminarCliente(){
		Cliente cliente1=new Cliente("Arturo","73131657Y",2/06/2018);
		
		unGestor.getV2().add(cliente1);
		unGestor.eliminarCliente("73131657Y");
		boolean encontrado=unGestor.getV2().contains(cliente1);
		assertTrue(encontrado);
	} //Se intenta eliminar un cliente inexistente
	@Test
	public void testeliminarCliente2(){
		
		unGestor.eliminarCliente("55555Z");
		boolean encontrado=unGestor.getV2().contains(uncliente);
		assertFalse(encontrado);
	}
}
