import java.util.ArrayList;
import java.util.Scanner;
public class Factura {
	 int codigoFactura;
	int fecha;
	String nombreProducto;
	int precioUnidad;
	int cantidad;
	Cliente cliente;
	Factura(){
		 int codigoFactura;
		int fecha;
		String nombreProducto;
		int precioUnidad;
		int cantidad;
		Cliente cliente;
	}
	Factura(int codigoFactura, int fecha,String nombreProducto, int precioUnidad, int cantidad,Cliente cliente){
		this.codigoFactura=codigoFactura;
		this.fecha=fecha;
		this.nombreProducto=nombreProducto;
		this.precioUnidad=precioUnidad;
		this.cantidad=cantidad;
		this.cliente=cliente;
	}
	
	
	public  int getCodigoFactura() {
		return codigoFactura;
	}
	public void setCodigoFactura(int codigoFactura) {
		this.codigoFactura = codigoFactura;
	}
	public int getFecha() {
		return fecha;
	}
	public void setFecha(int fecha) {
		this.fecha = fecha;
	}
	public String getNombreProducto() {
		return nombreProducto;
	}
	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}
	public int getPrecioUnidad() {
		return precioUnidad;
	}
	public void setPrecioUnidad(int precioUnidad) {
		this.precioUnidad = precioUnidad;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	@Override
	public String toString() {
		return "Factura [codigoFactura=" + codigoFactura + ", fecha=" + fecha + ", nombreProducto=" + nombreProducto
				+ ", precioUnidad=" + precioUnidad + ", cantidad=" + cantidad + ", cliente=" + cliente + "]";
	}
	
	
}
