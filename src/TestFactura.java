import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestFactura {
static GestorContabilidad otroGestor;
static GestorContabilidad otroGestor2;
Factura unafactura;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("Empezando tests");
		otroGestor=new GestorContabilidad();
		otroGestor2=new GestorContabilidad();
		Factura unafactura=new Factura(1,02/03/2018,"Consolas",100,28,null);
	}
@Before
public void limpiar(){
	otroGestor.getV().clear();
}
	
	public static void tearDownAfterClass() throws Exception {
		System.out.println("Acabando tests");
	}

	
//Busca una factura que no existe
	@Test
	public void  TestbuscarFactura() {
		otroGestor.getV().add(unafactura);
		Factura actual=otroGestor.buscarFactura(2);
		assertNull(actual);
	}
	//Busca una factura existente 
	@Test
	public void  TestbuscarFactura2() {
		Factura otrafactura=new Factura(3,02/03/2018,"Consolas",100,28,null);
		otroGestor.getV().add(otrafactura);
		otroGestor.buscarFactura(3);
	boolean actual=otroGestor.getV().contains(otrafactura);
	assertTrue( actual);
	}
	//Crea una Factura que no se encontraba en el array
	@Test
	public void TestcrearFactura(){
		Factura unaFactura2= new Factura (4,02/03/2018,"Consolas",100,28,null);
		otroGestor.getV().add(unaFactura2);
		otroGestor.crearFactura(unaFactura2);
		boolean encontrado=otroGestor.getV().contains(unaFactura2);
		assertTrue(encontrado);
	}
	//Se crea una factura que ya existia en el array
		@Test
		public void TestcrearFactura2(){
			Factura unaFactura2= new Factura (4,02/03/2018,"Consolas",100,28,null);
			otroGestor.crearFactura(unaFactura2);
			boolean encontrado=otroGestor.getV().contains(unaFactura2);
			assertFalse(encontrado);
		}
		//Se compruba entre las dos facturas existentes
		@Test
		public void TestfacturaMasCara(){
			Factura otraFactura= new Factura (4,02/03/2018,"Consolas",100,28,null);
			Factura otrafactura=new Factura(3,02/03/2018,"Consolas",100,28,null);
			otroGestor.getV().add(otrafactura);
			otroGestor.getV().add(otraFactura);
			Factura esperado=otroGestor.buscarFactura(4);
			Factura actual=otroGestor2.facturaMasCara();
			assertEquals(esperado,actual);
		}
		//Se espera un ingreso de 9999 en el a�o 2018
		@Test
		public void TestcalcularFacturacionAnual(){
			
			
			int esperado=9999;
			otroGestor.calcularFacturacionAnual(2018);
			int actual=9999;
			assertEquals(esperado, actual);
		}
		
		//se elimina ya que existe
		@Test
		public void TesteliminarFactura(){
			
			Factura unaFactura2= new Factura (4,02/03/2018,"Consolas",100,28,null);
			otroGestor.getV().add(unaFactura2);
			otroGestor.eliminarFactura(4);
			boolean actual=	otroGestor.getV().contains(unaFactura2);
			
			assertTrue(actual);
		}
		//No se elimina ya que no existe
				@Test
				public void TesteliminarFactura2(){
					
					Factura unaFactura2= new Factura (4,02/03/2018,"Consolas",100,28,null);
					otroGestor.eliminarFactura(8);
					boolean actual=	otroGestor.getV().contains(unaFactura2);
					
					assertFalse(actual);
				}
}

